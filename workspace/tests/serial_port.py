import serial
import time

port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=1.0)

i = 0

while True:
    port.write(bytearray(f"i = {i}", "utf-8"))
    time.sleep(1)
    print(f"Received: {repr(port.read(128))}")
    time.sleep(1)
    i += 1
