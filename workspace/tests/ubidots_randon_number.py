import random
import time

from ubidots import ApiClient

api = ApiClient(token='BBFF-VvU4C5Xxsh5FMfUQSOzGPaxFrACcFV')
random_number = api.get_variable('5df36f831d847210bc434938')

while (True):
    num_drawn = random.randint(1, 100)
    resp = random_number.save_value({'value':num_drawn})
    print(f"'num_drawn' ({num_drawn}) has been sent to IoT dashboard.")
    time.sleep(1)
