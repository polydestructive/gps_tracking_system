import gps
import time

from ubidots import ApiClient

gpsd_session = gps.gps('localhost', '2947')
gpsd_session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

ubisoft_api = ApiClient(token='BBFF-VvU4C5Xxsh5FMfUQSOzGPaxFrACcFV')
gps_data = ubisoft_api.get_variable('5df389a41d847243314c7f13')

while True:
    try:
        time.sleep(0.5)
        raw_data = gpsd_session.next()
        if raw_data['class'] == 'TPV':
            if hasattr(raw_data, 'lat') and hasattr(raw_data, 'lon'):
                latitude = raw_data.lat
                longitude = raw_data.lon
                print(f'Latitude: {str(latitude)}')
                print(f'Longitude: {str(longitude)}')
                resp = gps_data.save_value({'value':0, 'context':{'lat':latitude, 'lng':longitude}})
                print(resp)
    except KeyError:
        pass
    except KeyboardInterrupt:
        quit()
    except StopIteration:
        gpsd_session = None
        print('GPSD has terminated.')
